---
layout: rule
title: Trias politica
permalink: /templates/trias_politica/
icon: /assets/tabler_icons/dots.svg

community-name: 

# BASICS
structure: Three distinct and countervailing entities carry out executive, legislative, and judicial functions, respectively.
mission: 
values: Any source of authority should be kept in check through a separation of powers.
legal: 

# PARTICIPANTS
membership: New members may join according to the community's policies.
removal: The executive may remove a member for violating the community's policies at least twice.
roles:
limits: No one member may hold a role on the board, administration, or court simultaneously. Each elected role may be held subsequently for no more than two terms.

# POLICY
rights: 
decision: A 5-member board is responsible for maintaining the list of community policies. To adopt a change to the policies, it must receive win approval from 3 board members. Community members elect the board annually.
implementation: An administrator is responsible for implementing community policies, or delegating the implementation thereof. Community members elect the administrator annually.
oversight: A 3-member court can act on community-member complaints. If a policy is found to contradict this Rule, the court can overrule it with 2 votes. If the administrator is found to be failing to implement a policy or this Rule, the court can remove the administrator and call a new election to finish the term. Upon completing an annual term, the administrator may appoint one court member, replacing the longest-serving one.

# PROCESS

# EVOLUTION
records: 
modification: Changes to this Rule require 4 votes from the board and 2 votes from the court.
---

Use this template as-is or edit it to adapt it to your community.

A structure for complex, multi-stakeholder organizations, based on structures frequently found among national governments.
