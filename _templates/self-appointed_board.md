---
layout: rule
title: Self-appointed board
permalink: /templates/self-appointed_board/
icon: /assets/tabler_icons/user-plus.svg

community-name:

# BASICS
structure: An annually self-appointed board of no more than 5 members determines policies and implements them.
mission:
values:
legal:

# PARTICIPANTS
membership: The board's policies determine the criteria for community membership.
removal: The board may choose to remove a community member if at least 51% of board members vote for removal.
roles: Each year current board members elect the board members for the subsequent year.
limits: Board members can serve an unlimited number of 1-year terms.

# POLICY
rights: The board is expected to operate according to this Rule and the explicit agreements it has created.
decision: At least 51% of board members must vote to approve a proposal in order for it to become a community agreement.
implementation: Board members or their designees are responsible for implementing agreements, and the board should have access to the means to implement the agreements that it has approved.
oversight: Community participants unhappy with the board's implementation with the agreements may voice their concerns to individual board members or in the community at large.

# PROCESS
access: 
economics: 
deliberation: 
grievances: 

# EVOLUTION
records:
modification: Changes to this Rule require 2/3 approval of the board.
---

Use this template as-is or edit it to adapt it to your community.

A widely used governance structure for membership organizations whose members don't have capacity for active involvement in governance.
