# CommunityRule

---

A governance toolkit for great communities, located at [communityrule.info](https://communityrule.info).

---

This project welcomes contributors. All contributions are assumed to accept the project's GPL and Creative Commons licenses.

To contribute governance templates, copy create.md at _create/[template_name].md and fill out the YAML metadata. This will automatically add a new template into the system.

Propose edits to existing governance templates at _create/[template_name].md.

Most of CommunityRule's interactive features occur at _layouts/rule.html. The Module data is located at _data/modules.csv.
