---
layout: module
title: Delegation
permalink: /modules/delegation/
summary: Participants allocate their power to a specific representative for a fixed period or a specified role.
type: process
---

Delegative democracy allows voters to assign their voting power to others, generally on a per-issue basis and subject to immediate recall. Voters generally have the option of voting on a given question directly but can choose a proxy whom they deem trustworthy. That proxy may choose a proxy in turn.

Delegation has more recently, and [sometimes controversially](https://uniteddiversity.coop/2013/07/19/liquid-democracy-is-not-delegative-democracy/), been referred to as "liquid democracy." It occupies a middle space between systems of [representation](representation.md) and [referendum](referendum.md)-based direct democracy.

**Input:** Choice between referrendum voting and assigning a proxy, issue types

**Output:** Referendum participants with varying proxy power on each given issue type

## Background

[Bryan Ford](http://bford.info/2014/11/16/deleg.html):

> Perhaps unsurprisingly, I was only one of many people to think along these lines over the decades. In Lewis Carrol’s Principles of Parliamentary Representation of 1884, candidates could “club” their votes together, allowing a candidate who received more votes than required to win a seat to delegate the “excess” votes toward the election of other candidates. James C. Miller in 1969 foresaw electronic voting and proposed a delegate proxy voting scheme, to enable more widespread——and more propoportional——representation in political decisions.
> 
> Starting around 2002 the Internet exploded with reinventions of this and similar ideas. Besides my Delegative Democracy proposal, there was Dennis Lomax’s Beyond Politics, Jio Ito’s Emergent Democracy, Sayke’s Liquid Democracy and voting system, Mikael Nordfors’ Democracy 2.1, James Green-Armytage’s delegable proxy system, and Mark Rosst’s Structural Deep Democracy. The idea of allowing voters to delegate their participation to individually-chosen representatives is central to all of these proposals, though details differ.

## Feedback loops

### Sensitivities

* Amplifies voices of popularly recognized experts
* Disaggregates issues that might otherwise be bound together by party alliances

### Oversights

* Popularity may accord to proxies for reasons other than genuine expertise
* Lack of stability in proxy power might unsettle deliberative processes

## Implementations

### Communities

* [Flux](https://voteflux.org/) - Australian political party
* [Partido de la Red](http://partidodelared.org/) - Buenos Aires political party
* Pirate Parties in many countries have used delegation to decide on policies
* United States electoral candidates
    - [Michael Allman](https://allmanforcongress.com/) (US House, California)
    - [Camilo Casas](https://motherboard.vice.com/en_us/article/59dnbb/colorado-political-candidate-promises-to-give-his-seat-to-an-app) (city council, Boulder, CO)
    - [David Ernst](https://www.liquiddavid.com/) (US House, California)

### Tools

* [Democracy.Earth](https://democracy.earth) - Provides a blockchain-based liquid democracy system, from the creators of DemocracyOS
* [DemocracyOS](http://democracyos.org/)
* [LiquidFeedback](https://liquidfeedback.org/)
* [Liquid US](https://liquid.us/)

## Further resources

* Ford, Bryan. "[Delegative Democracy Revisited](http://bford.info/2014/11/16/deleg.html)." November 16, 2014.
* Hardt, Steve and Lia C. R. Lopes. “[Google Votes: A Liquid Democracy Experiment on a Corporate Social Network](http://www.tdcommons.org/cgi/viewcontent.cgi?article=1092&context=dpubs_series).” Technical Disclosure Commons, 2015.
* Sayke. "[Liquid Democracy In Context or, An Infrastructuralist Manifesto](https://web.archive.org/web/20160403043216/https://seed.sourceforge.net/ld_k5_article_004.html)." Retrieved April 3, 2016.
