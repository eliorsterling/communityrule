---
layout: module
title: Rough Consensus
permalink: /modules/rough_consensus/
summary: Decisions are made not through formal processes but through an informal sense of the group's shared will.
type: decision
---
