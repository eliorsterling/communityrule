---
layout: module
title: Fact-finding
permalink: /modules/fact-finding/
summary: An investigation into the facts of a matter.
type: process
---

Fact finding is a procedure that takes place in a variety of settings, primarily disputes, to investigate the true information about a given situation. Fact finding can occur in numerous ways, from interviewing potential participants to reviewing relevant documents. 

**Input:** An individual or team of individuals who are objective and impartial to the situation under review; a set procedure for fact finding; resources for investigation (i.e., interview questions)

**Output:** A collection of information designed to inform the parties engaged in a dispute

## Background

The term “fact finding” is relatively young, with its first recorded use occurring in the early 1900s. The Hague Convention of 1907 was the first time when official procedural measures were established for fact finding, specifically in the context of disputes between two states. Though it has evolved greatly to its current state, this version of the procedure served as a crucial model for later iterations.

In 1992, United Nations created an [updated list]( http://hrlibrary.umn.edu/monitoring/chapter18.html) of model procedures for fact finding in situations involving potential human rights violations based upon their 1970 Draft Model Rules.

## Feedback loops

### Sensitivities

* Joint fact finding can allow opposing sides an opportunity to work together despite their dispute
* Establishing facts can allow the dispute to become more stable and realistic
* In searching for facts, more information than the fact finder originally sought can be brought to light and provide more clarity to the situation

### Oversights

* Factual inquiries can easily be blended with values concerns, compromising impartiality and subjectivity
* Facts may not be the core of the issue at hand; interpretation of facts, relevance of facts, and how the facts fit into the larger dispute are largely subjective and may complicate the presence of bare facts
* Factual information may be difficult to find and, if found, may be complex and difficult to apply neutrally to the dispute

## Implementations

### Communities

Fact finding groups exist on both a national and an [international scale]( https://www.ihffc.org/index.asp?Language=EN&page=home). Numerous [government agencies]( https://www.eeoc.gov/federal/adr/factfinding.cfm) have fact finding commissions, including the Department of the Air Force, Department of Treasury, and the National Security Agency. In workplace disputes, human resources departments are generally assigned the responsibility of fact finding.

### Tools

* CPHR Alberta offers a step-by-step [guide]( https://www.cphrab.ca/report-writing-hr-professionals-conducting-workplace-investigations) to fact-finding in workplace scenarios
* Queens University IRC provides [six golden rules]( https://irc.queensu.ca/articles/golden-rules-fact-finding-six-steps-developing-fact-finding-plan) of fact-finding
* The International Bar Association shares the [International Human Rights Fact-Finding Guidelines]( https://www.ibanet.org/Fact_Finding_Guidelines.aspx) for download

## Further resources

Schultz, N. (2004, updated 2018). ["Fact-Finding."]( https://www.beyondintractability.org/essay/fact-finding/#narrow-body) Beyond Intractability. Eds. Guy Burgess and Heidi Burgess. Conflict Information Consortium, University of Colorado, Boulder.

Ramcharan, B. G. (1983). International law and fact-finding in the field of human rights. The Hague;Hingham, MA, USA;Boston;: M. Nijhoff Publishers.

[“What is Fact-Finding? Definition and Examples.”]( https://marketbusinessnews.com/financial-glossary/fact-finding/) Market Business News.

[Declaration on Fact-finding by the United Nations in the Field of the Maintenance of International Peace and Security](https://www.un.org/documents/ga/res/46/a46r059.htm). (1991). United Nations.
