---
layout: module
title: Debate
permalink: /modules/debate/
summary: Deliberation occurs through a structured presentation of arguments.
type: process
---

Debate is an activity in which two parties discuss opposing sides of an issue, event, or situation to argue a viewpoint. Often occurring in formal settings, debates frequently involve an audience and a moderator, though these features are not absolutely crucial to the process. Logic, credibility, and emotional appeal form the foundation for a well-formed, productive debate.

**Input:** parties willing to engage in civic discussion; an issue with relevant yet opposing viewpoints

**Output:** a formal discussion or conversation to argue a particular purpose

## Background

The most well-known history of debate goes back to ancient Greece and Rome when political opponents went head-to-head in public settings. Debate was also an important aspect of ancient Indian civilization; philosophical debates surrounding the meaning of scripture emerged as the Shastrartha. 
The first televised U.S. presidential debate took place between Richard Nixon and John F. Kennedy in 1960.

## Feedback loops

### Sensitivities

* Debates require an issue to be broken down into their foundational parts, leading to a greater understanding of it on a fundamental level 
* The information communicated through a debate can allow the audience more agency to make well-informed decisions 
* Organizations and governments can improve electoral and decision processes through engaging in debate that better informs their actions

### Oversights

* Critics of debate find that complex issues can easily be reduced into black and white answers; political debates automatically divide issues along partisan lines which can lead to a mere reinforcement of preconceived notions based on political leanings
* Finding a completely neutral moderator can be a near-impossible task that affects the outcome of the debate based on connotative phrasing of questions and biased facilitation 
* Cherry-picking facts to support a position can easily lead to misrepresentation of the issue

## Implementations

### Communities

Debates can take place in many settings. Political debates allow candidates to demonstrate and support their views on particular issues. Debate teams help students hone skills in argumentation, public speaking, and research. [Organizations can debate](https://mason.gmu.edu/~smuir/debate.htm) internal situations to reach greater agreement about a situation and decide on important issues. 

* The American Physical Therapy Association hosts an annual [Oxford Debate](https://www.apta.org/PTinMotion/News/2017/6/30/OxfordDebate/)
* Individuals running for organizational boards, such [Board of Trustees at Michigan State University](https://www.wkar.org/post/msu-board-trustees-debate-record-october-12-2018#stream/0), often engage in debate 

### Tools

* A variety of resources exist online to help student debaters, including outlines and templates like the [Chicago Debates resource guide](https://resources.chicagodebates.org/debaters/debate-resources/) 
* Visual resources can help map out logic and arguments
    * [Debatemap.live](https://debatemap.live/) is a site that breaks down arguments into “nodes,” or simplified pieces of information that connect to form a visual representation of the argument
    * [Argunet](http://www.argunet.org/editor/) is an open source software that allows users to create maps of their debates as individuals or teams 
* Nonprofit, nonpartisan site [Pro/Con](https://www.procon.org/) offers opposing viewpoints and information on frequently debated issues
* Online spaces allow community members to debate current issues, like the New York Times page [Room for Debate](https://www.nytimes.com/roomfordebate)

## Further resources

NYC Urban Debate League. (2017). History's Great Debates. Retrieved July 10, 2019, from https://blog.debate.nyc/historical-debates

How to Debate. (n.d.). Retrieved July 10, 2019, from https://www.sfu.ca/cmns/130d1/HOWTODEBATE.htm
Commission on Presidential Debates. [Resource on U.S. Presidential Debates]. Retrieved from https://www.debates.org/
