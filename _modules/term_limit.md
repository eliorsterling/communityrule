---
layout: page
title: Term limits
permalink: /modules/term_limits/
summary: Roles can be held for a maximum of a fixed period of time.
type: process
---

Term limits are a mechanism for governance systems to prevent one individual from being in office for such a period of time that prevents others from participating in governance. They exist as a way to balance power, protect democracy, and prevent dictatorship.

**Input:** agreement on what type of term limits should be imposed and for what amount of time:
* consecutive term limits mean individuals may serve again after a select number of years
* lifetime limits mean an individual may not serve again after reaching the term limit

**Output:** governance system that has regular turnover and is inclusive of new individuals’ voices

## Background

In ancient Greece, many Athenian officials under the Boule legislature, or legislature of 500 citizens, were limited to one term, or one year of serving to allow more members of the society to participate in governance. Certain Roman systems also imposed term limits to one year of service for consuls and magistrates.

In the United States, presidential term limits were debated and amendments were drafted more than 200 times between George Washington’s second presidential term to the actual passage of the 22nd amendment in 1951, when Franklin D. Roosevelt’s fourth term raised concerns once again. In fact, term limits were actually omitted from the Articles of Confederation when the Constitution was created. Many democratic republics now impose term limits while parliamentary governments are much less likely to do so.

## Feedback loops

### Sensitivities

* Prevents one individual from becoming a de facto dictator; prevents a legislature of career politicians in favor of a citizen’s legislature
* Officials may be more likely to accomplish more for the people they are serving if they are not constantly focused on re-election campaigns
* Without term limits, incumbents are extremely difficult to overturn due to connections and resources
* Decreases risk of corruption
* Brings new ideas and fresh perspectives into governance

### Oversights

* Long-term officials with citizen approval may be booted out of office due to a rule rather than their qualifications or lack of approval
* Officials who have been in office for extended time may have better experience with the system and be able to impact more productive change due to their large pool of knowledge
* Without the pressure of re-election, officials may be less likely to accomplish important tasks towards the end of their term

## Implementations

### Communities

Many democracies with presidential figures impose term limits, including the U.S., Colombia, Haiti, and within the past three decades, many African countries such as Algeria, Burundi, and the Democratic Republic of Congo.

Concerns for governance systems lacking term limits extend to organizations and corporations as well, as many corporate board positions are seen as lifetime tenures – and this is, for many investors, [a cause for concern](https://www.forbes.com/sites/patricialenkov/2018/08/28/the-why-when-and-how-of-board-refreshment/#4111e17f6fc9), as less than 10% of large companies have age or term limits. [Nonprofit boards](https://www.boardeffect.com/blog/best-practices-nonprofit-board-term-limits/) face a similar situation and often limit service to two consecutive terms or six years total. 

The Sierra Club [constitution](https://www.sierraclub.org/sites/www.sierraclub.org/files/Bylaws-Standing-Rules.pdf), for example, states that:
“Each Director shall be elected for a term of three (3) years. A Director may serve for an unlimited number of terms, providing that a minimum period of one year's absence from the Board shall occur after any two (2) consecutive full three-year terms.”

### Tools

* Sample clauses for establishing term limits can be found on [this](https://www.lawinsider.com/clause/term-limits) Law Insider page.
* [This](https://www.boardeffect.com/blog/best-practices-nonprofit-board-term-limits/) page from Board Effect offers best practices for implementing nonprofit board term limits.

## Further resources

Lenkov, P. (2018). “[The Why, When, and How of Board Refreshment](https://www.forbes.com/sites/patricialenkov/2018/08/28/the-why-when-and-how-of-board-refreshment/#4111e17f6fc9).” Forbes.

Nili, Y., Rosenblum, D. (2019). [Board Diversity by Term Limits?]( https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3353493). 71 Alabama Law Review; Univ. of Wisconsin Legal Studies Research Paper No. 1467. 

Baturo, A., & Elgie, R. (Eds.). (2019). The Politics of Presidential Term Limits. Oxford University Press.

Klašnja, M., & Titiunik, R. (2017). The incumbency curse: Weak parties, term limits, and unfulfilled accountability. American Political Science Review, 111(1), 129-148.


