---
layout: module
title: Platform
permalink: /modules/platform/
summary: A statement expresses the commitments of a faction contending for authority.
type: process
---


<!--
As in, party platforms, clarifying what a party stands for

See also solidarity
-->
