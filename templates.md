---
layout: page
title: Templates
permalink: /templates/
---

{% for rule in site.templates %}
<h2>
    <img src="{{ site.baseurl }}{{ rule.icon }}" />
    <a href="{{ site.baseurl }}{{ rule.url }}">{{ rule.title }}</a>
</h2>
<p>{{ rule.structure }}</p>
{% endfor %}
