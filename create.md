---
layout: rule
title: Create
permalink: /create/
icon: 

community-name: 

# BASICS
structure: 
mission:
values:
legal: 

# PARTICIPANTS
membership:
removal:
roles:
limits: 

# POLICY
rights: 
decision: 
implementation: 
oversight: 

# PROCESS
deliberation: 
access: 
economics: 
grievances: 

# EVOLUTION
records: 
modification: 
---

Create a Rule from scratch, using modules and/or answering the questions in the drop-downs below. Answer questions in complete sentences—the questions disappear.
